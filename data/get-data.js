require('dotenv').config()
var request = require('superagent')
var Throttle = require('superagent-throttle')
var eachLimit = require('async/eachLimit')
var battleNet = require('../common/battle-net')
var constants = require('../common/constants')
var moment = require('moment')
var massive = require('massive')
var jsonfile = require('jsonfile')

var throttle = new Throttle({
  active: true,
  rate: 1,
  ratePer: 3000,
  concurrent: 1
})

var connString = 'postgres://' + process.env.DB_USER + ':' + encodeURIComponent(process.env.DB_PASS) + '@' + process.env.DB_HOST + ':' + process.env.DB_PORT + '/' + process.env.DB_NAME
massive.connect({ connectionString: connString }, function (err, db) {
  if (err) {
    console.log('failed to connect to db')
    return
  }
  var characters = jsonfile.readFileSync('./data/list.json')
  eachLimit(characters, 1, function (character, callback) {
    console.log(moment().format('DD-MM HH:mm:ss'), character)
    request
      .get(battleNet.buildURL(
        encodeURIComponent(character.name),
        encodeURIComponent(character.realm)
      ))
      .use(throttle.plugin())
      .end(function (err, res) {
        if (err) {
          callback(err)
          console.log(err)
        }

        var character = battleNet.parseResponse(res.body)
        var exists = db.characters.findOneSync({ name: character.name })
        if (exists === undefined) {
          character.updated_at = moment().toDate()
          db.characters.insertSync(character)
          callback()
        } else {
          db.characters.updateSync({
              id: exists.id,
              name: character.name,
              equipped_ilvl: character.equipped_ilvl,
              average_ilvl: character.average_ilvl,
              artifact_power: character.artifact_power,
              artifact_traits: character.artifact_traits,
              artifact_ilvl: character.artifact_ilvl,
              artifact_name: character.artifact_name,
              last_recorded_spec: character.last_recorded_spec,
              level: character.level,

          })
          callback()
        }
      })
  }, function (err) {
    if (err) {
      console.log('Failed to get character')
    }
    console.log(moment().format('DD-MM HH:mm:ss') + ' - Finished datamining')
  })
})
