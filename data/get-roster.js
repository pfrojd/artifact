require('dotenv').config()
var request = require('superagent')
var battleNet = require('../common/battle-net')
var constants = require('../common/constants')
var massive = require('massive')
var jsonfile = require('jsonfile')

var connString = 'postgres://' + process.env.DB_USER + ':' + encodeURIComponent(process.env.DB_PASS) + '@' + process.env.DB_HOST + ':' + process.env.DB_PORT + '/' + process.env.DB_NAME
var db = massive.connectSync({ connectionString: connString })

request
  .get(battleNet.buildRosterURL('Aeon'))
  .end(function (err, res) {
    if (err) {
      console.log(err)
    }
    var roster = battleNet.parseRosterResponse(res.body.members)
    jsonfile.writeFile('./data/list.json', roster, function (err) {
      if (err) {
        console.log(err)
      }
      console.log('Wrote to file')
    })
  })
