var moment = require('moment')
var service = {}

service.createEmptyCharacter = function () {
  return {
    name: undefined,
    updated_at: new Date(),
    equipped_ilvl: 0,
    average_ilvl: 0,
    artifact_power: 0,
    artifact_traits: 0,
    artifact_ilvl: 0,
    artifact_name: undefined,
    level: 0
  }
}

service.findCharacter = function (db, character, callback) {
  db.characters.findOne({ name: character.name }, { stream: true }, function (err, stream) {
    if (err) {
      console.log('error', err)
    }
    callback(char)
  })
}

service.insertCharacter = function (db, character, callback) {
  character.updated_at = moment().toISOString()
  db.characters.insert(character, function (err, res) {
    if (err) {
      console.log('error', err)
    }
    console.log('Successfully inserted character')
  })
}

service.updateCharacter = function (db, character, id, callback) {
  character.updated_at = moment().toISOString()
  db.characters.update({ id: id }, character, function (err, res) {
    if (err) {
      console.log('error', err)
    }
    console.log('Successfully updated character')
  })
}

module.exports = service
