var _ = require('lodash')
var constants = require('./constants')
var service = require('./service')
var moment = require('moment')

var battleNet = {}

battleNet.buildURL = function (characterName, realm) {
  return constants.BASE_URL + 'character/' + realm + '/' + characterName + '?' + 'fields=talents,items,achievements&' + constants.LOCALE + '&apikey=' + constants.KEY
}

battleNet.buildRosterURL = function (guildName) {
  return constants.BASE_URL + 'guild/' + constants.REALM_NAME + '/' + guildName + '?' + 'fields=members&' + constants.LOCALE + '&apikey=' + constants.KEY
}

battleNet.parseRosterResponse = function (res) {
  var listOfChars = res.filter(function (char) {
    if (char.rank <= 4 && char.rank !== 2) {
      return char
    }
  })

  return listOfChars.map(function (char) {
    return {
      name: char.character.name,
      realm: char.character.realm
    }
  })
}

// This is a retarded implementation, and made when I was very tired
// Needs to be rewritten, but its also partly Blizzards fault for not
// implementing a property for the characters current spec (and only defining
// which of the talent sets is selected)
battleNet.getSpecialization = function (talents) {
  var specName
  _.map(talents, function (spec) {
    if (spec.selected) {
      _.map(spec.talents, function (talent) {
        if (talent.spec !== undefined && talent.spec !== null) {
          if (specName === undefined) {
            specName = talent.spec.name
          }
        }
      })
    }
  })
  return specName
}

battleNet.parseResponse = function (res) {
  var character = service.createEmptyCharacter()
  character.name = res.name
  character.level = res.level
  character.equipped_ilvl = res.items.averageItemLevelEquipped
  character.average_ilvl = res.items.averageItemLevel
  character.last_recorded_spec = this.getSpecialization(res.talents)
  console.log(moment().format('DD-MM HH:mm:ss'), 'Determined spec to be ' + character.last_recorded_spec)

  var artifact
  if (res.items.mainHand.artifactId !== 0) {
    artifact = res.items.mainHand
  }

  if (res.items.mainHand.artifactId === 0) {
    artifact = res.items.offHand
  }

  if (artifact) {
    character.artifact_ilvl = artifact.itemLevel

    // Results seem odd from this, because it's kind of unknown whether the traits returned
    // from the response include golden traits (and the initial 1 trait)
    // var artifactTraitsAmount = _.reduce(artifact.artifactTraits, function (sum, trait) {
    //   return sum + trait.rank
    // }, 0)

    // Remove relics.
    // artifactTraitsAmount = artifactTraitsAmount - artifact.relics.length

    // Instead of checking the item (which is unreliable) we can check the traitlevel
    // from checking an achievement. Thanks to dev forums for this.
    var pos = res.achievements.criteria.indexOf(29395)
    var artifactTraitsAmount = res.achievements.criteriaQuantity[pos]

    var totalAP = _.reduce(constants.ARTIFACT_TRAIT_TABLE, function (sum, value, key) {
      if (artifactTraitsAmount >= key) {
        return sum + value
      }
      return sum
    }, 0)
    character.artifact_power = totalAP
    character.artifact_traits = artifactTraitsAmount
    character.artifact_name = artifact.name
  } else {
    console.log('Character ' + res.name + ' is missing a valid artifact', res.items.mainHand.name)
    character.artifact_name = 'Missing'
  }
  return character
}

module.exports = battleNet
