require('dotenv').config()
var express = require('express')
var helmet = require('helmet')
var app = express()
var http = require('http')
var bodyParser = require('body-parser')
var methodOverride = require('method-override')
var massive = require('massive')

app.use(helmet())
var connString = 'postgres://' + process.env.DB_USER + ':' + encodeURIComponent(process.env.DB_PASS) + '@' + process.env.DB_HOST + ':' + process.env.DB_PORT + '/' + process.env.DB_NAME
var massiveInstance = massive.connectSync({ connectionString: connString })

// Set a reference to the massive instance on Express' app:
app.set('db', massiveInstance)

app.use(bodyParser.urlencoded({
  extended: true
}))
app.use(bodyParser.json())
app.use(methodOverride())
app.use('/static', express.static(__dirname + '/app/public'))
app.set('views', __dirname + '/app')
app.set('view engine', 'pug')

app.get('/', function (req, res, next) {
  var db = req.app.get('db')
  db.characters.find(function (err, result) {
    if (err) {
    }
    res.render('index', {
      data: result
    })
  })
})

http.createServer(app).listen(8000)
console.log('server started on ' + 8000)
