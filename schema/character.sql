CREATE TABLE characters (
    id SERIAL,
    updated_at timestamp without time zone NOT NULL,
    name varchar(256) not null,
    equipped_ilvl integer not null,
    average_ilvl integer not null,
    artifact_power integer not null,
    artifact_ilvl integer not null,
    artifact_name varchar(256) not null,
    artifact_traits integer not null,
    last_recorded_spec varchar(256) not null,
    level integer not null,
    CONSTRAINT PK_Characters PRIMARY KEY (id)
);

CREATE OR REPLACE FUNCTION update_modified_column()
RETURNS TRIGGER AS $$
BEGIN
   IF row(NEW.*) IS DISTINCT FROM row(OLD.*) THEN
      NEW.updated_at = now();
      RETURN NEW;
   ELSE
      RETURN OLD;
   END IF;
END;
$$ language 'plpgsql';

CREATE TRIGGER updated_timestamp
  BEFORE UPDATE ON characters
  FOR EACH ROW
  EXECUTE PROCEDURE update_modified_column()
